package org.jboss.ddoyle.bpmsuite.workshop.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.jboss.bpms.workshop.ticket_datamodel.MyType;
import org.jboss.bpms.workshop.ticket_datamodel.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartProcessWithComplexObject {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StartProcessWithComplexObject.class);

	public static void main(String[] args) {
		URL bpmsUrl;
		try {
			bpmsUrl = new URL("http://localhost:8080/business-central");
		} catch (MalformedURLException mue) {
			throw new RuntimeException("Error building BPMSuite target URL.", mue);
		}

		BPMSuiteClient client = new BPMSuiteClient(bpmsUrl, "org.jboss.ddoyle.bpms.workshop:TicketProcess:1.0.0",
				"bpmsAdmin", "jboss@01");

		String processId = "TicketProcess.TicketProcess";
		Map<String, Object> params = new HashMap<>();

		Person person = new Person();
		person.setName("Duncan");
		person.setSurName("Doyle");
		person.setDateOfBirth(toXmlGregorianCalendar(new GregorianCalendar(1979, 1, 30)));
		
		MyType type = new MyType();
		type.setData(10);
		type.setText("Bla");

		params.put("person", person);

		client.startProcess(processId, params);
	}

	private static XMLGregorianCalendar toXmlGregorianCalendar(GregorianCalendar calendar) {
		XMLGregorianCalendar xmlCalendar = null;
		try {
			xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		} catch (DatatypeConfigurationException ex) {
			throw new RuntimeException("Error converting date to xml gregorian calendar.");
		}
		return xmlCalendar;
	}

}
