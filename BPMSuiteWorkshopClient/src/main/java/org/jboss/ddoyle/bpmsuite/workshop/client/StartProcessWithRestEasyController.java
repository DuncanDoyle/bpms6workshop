package org.jboss.ddoyle.bpmsuite.workshop.client;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link Controller} that uses the JBoss RESTEasy framework to call BPMSuite via REST and start a process.
 *  
 * @author <a href="mailto:duncan.doyle@redhat.com">Duncan.Doyle</a>
 */
//@Controller
public class StartProcessWithRestEasyController {

	private static final Logger LOGGER = LoggerFactory.getLogger(StartProcessWithRestEasyController.class);

	/**
	 * Handles the request.
	 */
	//@RequestMapping("/startProcessWithRestEasy.htm")
	public void startProcessHtm() {
		LOGGER.info("Starting process.");
		startProcess();
	}

	/**
	 * Handles the request.
	 */
	//@RequestMapping("/startProcessWithRestEasy.html")
	public void startProcessHtml() {
		LOGGER.info("Starting process.");
		startProcess();
	}

	private long startProcess() {

		ApacheHttpClient4Executor executor = new ApacheHttpClient4Executor();
		DefaultHttpClient client = (DefaultHttpClient) executor.getHttpClient();

		// Set the username and password on the HttpClient credentials provider.
		client.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
				new UsernamePasswordCredentials("ddoyle", "jboss@01"));

		// Create the HTTP Context
		BasicHttpContext localcontext = new BasicHttpContext();
		/*
		 * Generate BASIC scheme object and stick it to the local execution context. This will later be filled with the credentials.
		 */
		BasicScheme basicAuth = new BasicScheme();
		localcontext.setAttribute("preemptive-auth", basicAuth);
		// Set HTTP Context on the executor.
		executor.setHttpContext(localcontext);
		// Add the PreemptiveAuth interceptor as the first HttpRequestInterceptor.
		client.addRequestInterceptor(new PreemptiveAuth(), 0);

		ClientRequest request = new ClientRequest(
				"http://localhost:8080/business-central/rest/runtime/be.eandis:EandisBpm:1.0.0/process/EandisBpm.bpm-process/start",
				executor);

		request.accept("application/json");

		ClientResponse response;
		try {
			response = request.post();
		} catch (Exception e) {
			LOGGER.error("Caught exception.", e);
			throw new RuntimeException("Caught exception.", e);
		}
		if (response.getStatus() != 200) {

			LOGGER.error("Did not receive 200! Received: " + response.getStatus());
		}
		LOGGER.info("Process started.");
		
		return 0L;
	}

	/**
	 * Apache HTTP Client Request Interceptor which sets the HTTP Basic Auth Credentials on the request.
	 * 
	 * 
	 * @author <a href="mailto:duncan.doyle@redhat.com>Duncan Doyle</a>
	 */
	static class PreemptiveAuth implements HttpRequestInterceptor {

		public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {

			LOGGER.info("In Preemptive authentication processor.");

			AuthState authState = (AuthState) context.getAttribute(ClientContext.TARGET_AUTH_STATE);

			// If no auth scheme avaialble yet, try to initialize it preemptively
			if (authState.getAuthScheme() == null) {
				LOGGER.info("Initializing Authentication Scheme.");
				//Retrieve the authscheme to be used from the context. We've set this earlier when creating the request.
				AuthScheme authScheme = (AuthScheme) context.getAttribute("preemptive-auth");
				//TODO: We could check which scheme has been set to determine what kind of credentials to provide.
				// Retrieve the credentialsprovider from the context.
				CredentialsProvider credsProvider = (CredentialsProvider) context.getAttribute(ClientContext.CREDS_PROVIDER);
				HttpHost targetHost = (HttpHost) context.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
				if (authScheme != null) {
					LOGGER.info("Auth Scheme is not null.");
					// Retrieve the credentials for the current target host from the credentials provider.
					Credentials creds = credsProvider.getCredentials(new AuthScope(targetHost.getHostName(), targetHost.getPort()));
					if (creds == null) {
						throw new HttpException("No credentials for preemptive authentication");
					}
					// Set the auth scheme to be used.
					authState.setAuthScheme(authScheme);
					authState.setCredentials(creds);
				} else {
					LOGGER.info("Auth scheme already set.");
				}
			}

		}

	}
	
}
