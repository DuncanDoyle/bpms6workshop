package org.jboss.ddoyle.bpmsuite.workshop.client;

import java.net.URL;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.services.client.api.RemoteRestRuntimeEngineFactory;
import org.kie.services.client.api.RemoteRuntimeEngineFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BPMSuiteClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(BPMSuiteClient.class);

	private final URL bpmsUrl;
	private final String deploymentId;
	private final String userId;
	private final String password;
	
	private RuntimeEngine runtimeEngine = null;

	public static void main(String[] args) {
		
	}

	public BPMSuiteClient(final URL bpmsUrl, final String deploymentId, final String userId, final String password) {
		this.bpmsUrl = bpmsUrl;
		this.deploymentId = deploymentId;
		this.userId = userId;
		this.password = password;
	}

	public void startProcess(String processId, Map<String, Object> params) {
		RuntimeEngine runtimeEngine = getRuntimeEngine();
		KieSession ksession = runtimeEngine.getKieSession();
		ksession.startProcess(processId, params);
	}

	public void completeWorkItem(final long workItemId, final Map<String, Object> results) {
		LOGGER.info("Completing workItem with id: " + workItemId);
		
		RuntimeEngine runtimeEngine = getRuntimeEngine();
		runtimeEngine.getKieSession().getWorkItemManager().completeWorkItem(workItemId, results);
	}

	private synchronized RuntimeEngine getRuntimeEngine() {
		if (runtimeEngine == null) {
			RemoteRuntimeEngineFactory remoteRuntimeEngineFactory = RemoteRestRuntimeEngineFactory.newBuilder()
					.addDeploymentId(deploymentId).addUrl(bpmsUrl).addUserName(userId).addPassword(password).build();
			RuntimeEngine runtimeEngine = remoteRuntimeEngineFactory.newRuntimeEngine();
			this.runtimeEngine = runtimeEngine;
		}
		
		return runtimeEngine;
	}
}
