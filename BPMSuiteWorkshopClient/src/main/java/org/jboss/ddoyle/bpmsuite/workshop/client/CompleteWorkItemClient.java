package org.jboss.ddoyle.bpmsuite.workshop.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CompleteWorkItemClient {

	
	
	public static void main(String[] args) {
		URL bpmsUrl;
		try {
			bpmsUrl = new URL("http://localhost:49159/business-central");
		} catch (MalformedURLException mue) {
			throw new RuntimeException("Error building BPMSuite target URL.", mue);
		}

		BPMSuiteClient client = new BPMSuiteClient(bpmsUrl, "org.jboss.ddoyle.bpms.workshop:LongRunningBackendProcess:1.0.0",
				"bpmsAdmin", "jboss@01");

		//TODO: implement parameter checking.
		long workItemId = new Long(args[0]).longValue();
		Map<String, Object> results = new HashMap<>();
		results.put("accepted", Boolean.TRUE);
		client.completeWorkItem(workItemId, results);
	}
	
	
	
}
