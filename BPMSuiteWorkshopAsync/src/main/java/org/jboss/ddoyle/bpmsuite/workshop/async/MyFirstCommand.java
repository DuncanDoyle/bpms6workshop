package org.jboss.ddoyle.bpmsuite.workshop.async;

import java.util.Map;
import java.util.Set;

import org.kie.api.runtime.process.WorkItem;
import org.kie.internal.executor.api.Command;
import org.kie.internal.executor.api.CommandContext;
import org.kie.internal.executor.api.ExecutionResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class MyFirstCommand implements Command {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MyFirstCommand.class);
	
	private static final String MESSAGE_KEY = "message";
	
	private static final String WORKITEM_KEY = "workItem";

	@Override
	public ExecutionResults execute(CommandContext ctx) throws Exception {
		//We can retrieve the properties from the CommandContext, just like in a WorkItemHandler.
		Map<String, Object> data = ctx.getData();
		
		//List all the data in the context.
		LOGGER.info("CommandContext data map");
		Set<Map.Entry<String, Object>> entrySet = data.entrySet();
		for (Map.Entry<String, Object> nextEntry: entrySet) {
			String key = nextEntry.getKey();
			Object value = nextEntry.getValue();
			LOGGER.info("Key: " +  key + ", value: " + value );
		}
		
		//Get the 'message' workitem data.
		WorkItem workItem = (WorkItem) data.get(WORKITEM_KEY);
		String message = (String) workItem.getParameter(MESSAGE_KEY);
		LOGGER.info("Received async message: " + message);
		
		return new ExecutionResults();
	}
	
	
}
