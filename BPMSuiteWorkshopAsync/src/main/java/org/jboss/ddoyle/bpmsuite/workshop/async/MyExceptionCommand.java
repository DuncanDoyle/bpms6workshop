package org.jboss.ddoyle.bpmsuite.workshop.async;

import org.kie.internal.executor.api.Command;
import org.kie.internal.executor.api.CommandContext;
import org.kie.internal.executor.api.ExecutionResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command which deliberately fails to demonstrate exceptionhandling in jBPM6.  
 * 
 * @author <a href="mailto:duncan.doyle@redhat.com">Duncan Doyle</a>
 */
public class MyExceptionCommand implements Command {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyExceptionCommand.class);
	
	
	@Override
	public ExecutionResults execute(CommandContext ctx) throws Exception {
		LOGGER.info("Executing command and throwing exception.");
		
		throw new RuntimeException("Something went seriously wrong while calling service.");
		
		//return null;
	}

}
