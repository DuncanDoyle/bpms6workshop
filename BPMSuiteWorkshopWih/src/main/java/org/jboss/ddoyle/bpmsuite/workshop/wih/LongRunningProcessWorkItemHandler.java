package org.jboss.ddoyle.bpmsuite.workshop.wih;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simulates a long running process. I.e., it doesn't call complete on the WorkItem. Instead, completion of the process needs to be done by
 * an external process.
 * 
 * @author <a href="mailto:duncan.doyle@redhat.com">Duncan Doyle</a>
 * 
 */
public class LongRunningProcessWorkItemHandler implements WorkItemHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongRunningProcessWorkItemHandler.class);
	
	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		LOGGER.info("Executing long running process. WorkItem: " + workItem.getId());

	}
	
	
	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		LOGGER.info("Aborting long running process. We inform the backend that the long running process can be stopped");
	}

}
