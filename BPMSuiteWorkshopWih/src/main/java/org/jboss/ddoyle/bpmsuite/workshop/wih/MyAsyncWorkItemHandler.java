package org.jboss.ddoyle.bpmsuite.workshop.wih;

import org.jbpm.executor.ExecutorServiceFactory;
import org.jbpm.executor.impl.wih.AsyncWorkItemHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyAsyncWorkItemHandler extends AsyncWorkItemHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MyAsyncWorkItemHandler.class);

	public MyAsyncWorkItemHandler() {
		super(ExecutorServiceFactory.newExecutorService(null), MySecondCommand.class.getName());
		LOGGER.info("Created new MyAsyncWorkItemHandler.");
	}

}
