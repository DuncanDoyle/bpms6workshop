package org.jboss.ddoyle.bpmsuite.workshop.wih;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleWorkItemHandler implements WorkItemHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleWorkItemHandler.class);
	
	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		LOGGER.info("Aborting work item: " + workItem.getId());
		
	}

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		LOGGER.info("Executing work item: " + workItem);
		
	}
	
}
