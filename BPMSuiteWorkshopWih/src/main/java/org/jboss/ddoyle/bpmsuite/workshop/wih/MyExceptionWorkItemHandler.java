package org.jboss.ddoyle.bpmsuite.workshop.wih;


import org.jbpm.bpmn2.handler.WorkItemHandlerRuntimeException;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyExceptionWorkItemHandler implements WorkItemHandler{

	private static final Logger LOGGER = LoggerFactory.getLogger(MyExceptionWorkItemHandler.class);

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
		LOGGER.info("Executing workitem and throwing exception.");
		throw new WorkItemHandlerRuntimeException(null);
		
	}
	
	
	@Override
	public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
		// 
	}

}
