package org.jboss.ddoyle.bpmsuite.workshop.wih;

import org.kie.internal.executor.api.Command;
import org.kie.internal.executor.api.CommandContext;
import org.kie.internal.executor.api.ExecutionResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author <a href="mailto:duncan.doyle@redhat.com">Duncan Doyle</a>
 
 */
public class MySecondCommand implements Command {

	private static final Logger LOGGER = LoggerFactory.getLogger(MySecondCommand.class);
	
	@Override
	public ExecutionResults execute(CommandContext ctx) throws Exception {
		
		LOGGER.info("Executing my second command ..... asynchronously.");
		
		return new ExecutionResults();
	}

}
